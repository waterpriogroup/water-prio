from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
import time
import requests

API_KEY = "d8d89de21235e1fcab5be5ee3269541c"
ROOT_URL = "https://api.openweathermap.org/data/2.5/"

# Create your views here.

def get_next_rain(request, lat, lon):
    current_time = time.time()
    try:
        r = requests.get((ROOT_URL + "forecast?lat={lat}&lon={lon}&APPID={KEY}".format(lat=lat, lon=lon, KEY=API_KEY)))
        data = r.json()["list"]
        for d in data:
            if "rain" in d:
                if d["rain"]["3h"] >= int(request.GET.get('volume', 10)):
                    return JsonResponse({"data": int(d["dt"]) - int(current_time)})

        return JsonResponse({"data": -1})
    except Exception as e:
        print(repr(e))
        return HttpResponse(r.text)
